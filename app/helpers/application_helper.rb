module ApplicationHelper

  # Returns the full title
  def full_title(page_title = '')
    base_title = "Oxxy's Sample Ruby on Rails App"
	if page_title.empty?
	  base_title
	else
	  page_title + " | " + base_title
	end
  end

end
