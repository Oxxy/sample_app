require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @base_title = "Oxxy's Sample Ruby on Rails App"
  end
  
  test "should get root" do
    get root_url
	assert_response :success
  end
  
  test "should get home" do
    get root_path
    assert_response :success
	assert_select "title", "Home page | #{@base_title}"
  end

  test "should get help" do
    get help_path
    assert_response :success
	assert_select "title", "Help page | #{@base_title}"
  end
  
  test "should get about" do
    get about_path
	assert_response :success
	assert_select "title", "About page | #{@base_title}"
  end
  
  test "should get contact" do
    get contact_path
	assert_response :success
	assert_select "title", "Contact page | #{@base_title}"
  end
  
  test "should get signup" do
	get signup_path
	assert_response :success
	assert_select "title", "Sign up page | #{@base_title}"
  end

end
