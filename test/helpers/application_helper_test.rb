require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title, "Oxxy's Sample Ruby on Rails App"
    assert_equal full_title("Help page"), "Help page | Oxxy's Sample Ruby on Rails App"
	assert_equal full_title("About page"), "About page | Oxxy's Sample Ruby on Rails App"
	assert_equal full_title("Contact page"), "Contact page | Oxxy's Sample Ruby on Rails App"
	assert_equal full_title("Home page"), "Home page | Oxxy's Sample Ruby on Rails App"
	assert_equal full_title("Sign up page"), "Sign up page | Oxxy's Sample Ruby on Rails App"
  end
end